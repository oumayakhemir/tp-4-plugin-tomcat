pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                checkout([$class: 'GitSCM',
                    branches: [[name: '*/main']],
                    extensions: [],
                    userRemoteConfigs: [[credentialsId: 'jenkins-gitlab',
                        url: 'git@gitlab.com:garsaa.sofiene/tp6-jenkins-docker.git']]])
            }
        }

        stage('Build') {
            steps {
                sh 'mvn clean package'
                sh 'echo "Workspace Directory: ${workspace}"'
            }
        }

        stage('Create Docker image') {
            steps {
                sh "docker build -t soft2023/sofiene-garsaa-image:${env.BUILD_NUMBER} ."
            }
        }

        stage('Push to Docker Hub') {
            steps {
                withCredentials([string(credentialsId: 'docker-hub-credentials', variable: 'DOCKER_HUB_TOKEN')]) {
                    script {
                        def dockerHubToken = env.DOCKER_HUB_TOKEN
                        sh """
                            docker login --username=soft2023 --password="${dockerHubToken}"
                            docker push soft2023/sofiene-garsaa-image:${env.BUILD_NUMBER}
                        """
                    }
                }
            }
        }
        
        stage('Run Ansible Playbook') {
            steps {
                ansiblePlaybook becomeUser: 'jenkins',
                credentialsId: 'jenkins-user_jenkins',
                disableHostKeyChecking: true,
                installation: 'ansible',
                inventory: 'inventory.yml',
                playbook: 'dockerimage.yml'
 
            }
        }
    }
}
